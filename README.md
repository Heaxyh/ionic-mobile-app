# Welcome!

Hey there, this school project is a mobile app. Build with **Ionic**, **Net Core** and **EF Core**. 
It's really a basic app. Specially for the backend I hadn't enough time. So it exits but I could be better. 

## Installation 
1. Install all dependencies
>**Frontend**: `npm i`
>**InventoryAPI**: `Install-Package Microsoft.EntityFrameworkCore.SqlServer `
2. Create a Database in your **MS SQL Server**
>**SSMS**: `CREATE DATABASE Inventory`

## Start the app
1. Start the API
2. Navigate to **/Frontend** and run `ionic serve`

