import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {AuthState} from "../models/utils";

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    form = new FormGroup({
        username: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required)
    });

    constructor(private authService: AuthService) {
    }

    get username() {
        return this.form.get('username');
    }

    get password() {
        return this.form.get('password');
    }

    ngOnInit() {
    }

    onSubmit() {
        this.authService.login({username: this.username.value, password: this.password.value});
        this.authService.authState$.subscribe(
            (res: AuthState) => res === AuthState.authorized ? console.log(res) : console.log('fail +', res)
        );
        this.form.reset();

    }
}
