import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuardService} from "./services/auth-guard.service";

const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    },
    {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),
        canActivate: [AuthGuardService]
    },
    {
        path: 'inventory-browser',
        loadChildren: () => import('./inventory-browser/inventory-browser.module').then(m => m.InventoryBrowserPageModule),
        canActivate: [AuthGuardService]
    },
    {
        path: 'item-editor',
        loadChildren: () => import('./item-editor/item-editor.module').then(m => m.ItemEditorPageModule),
        canActivate: [AuthGuardService]

    },
    {
        path: 'login',
        loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
    }

];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
