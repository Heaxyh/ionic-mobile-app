import { Injectable } from '@angular/core';
import { Item } from '../models/item';

@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {

    constructor() {
    }

    private _item: Item;

    get item(): Item | null {
        const item: Item = this._item;
        this._item = null;
        return item;
    }

    set item(item: Item) {
        this._item = item;
    }
}
