import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthState } from '../models/utils';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
      private router: Router,
      private authService: AuthService) {
  }

  canActivate(): boolean {
    if (this.authService.state === AuthState.unathorized) {
      this.router.navigate(['/login']);
    }
    return this.authService.state === AuthState.authorized;
  }
}
