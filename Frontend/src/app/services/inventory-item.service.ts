import { Item } from '../models/item';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
    providedIn: 'root'
})
export class ItemService {

    private url: string;
    private headers: HttpHeaders = new HttpHeaders();

    constructor(private httpClient: HttpClient) {
        this.headers.set('Access-Control-Allow-Origin', '*');
        this.url = environment.inventoryApi + this.constructor.name.split('Service')[0];
    }

    getItems() {
        return this.httpClient.get<Item[]>(this.url, {headers: this.headers});
    }

    getItem(id: number) {
        return this.httpClient.get<Item | null>(`${this.url}/${id}`, {headers: this.headers});
    }

    deleteItem(id: number) {
        return this.httpClient.delete<any>(`${this.url}/${id}`, {headers: this.headers});
    }

    postItem(item: Item) {
        return this.httpClient.post<Item>(this.url, item);
    }

    updateItem(item: Item) {
        return this.httpClient.put<Item>(`${this.url}/${item.id}`, {headers: this.headers});
    }

}
