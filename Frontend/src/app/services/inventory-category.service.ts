import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Category } from '../models/category';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class CategoryService {

    private url: string;
    private headers: HttpHeaders = new HttpHeaders();

    constructor(private httpClient: HttpClient) {
        this.headers.set('Access-Control-Allow-Origin', '*');
        this.url = environment.inventoryApi + this.constructor.name.split('Service')[0];
    }

    getCategories() {
        return this.httpClient.get<Category[]>(this.url, {headers: this.headers});
    }

}
