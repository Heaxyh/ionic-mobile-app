import {Injectable} from '@angular/core';
import {AuthState, ILogin} from '../models/utils';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    authState$ = new BehaviorSubject(AuthState.unathorized);
    private url: string;
    private _key = 'LOGIN_STATE';


    constructor(private http: HttpClient,
                private router: Router,
                private storage: Storage) {
        this.url = environment.inventoryApi + this.constructor.name.split('Service')[0];
        //this.ifValid();
    }

    get state(): AuthState {
        return this.storage.get(this._key) ? AuthState.authorized : AuthState.unathorized;
    }

    set state(state: AuthState) {
        this.storage.set(this._key, state).then(st => this.authState$.next(st));
    }

    login(body: ILogin): Observable<AuthState> {
        const subject$ = new Subject<AuthState>();
        this.http.post(this.url, body).subscribe(
            (res: boolean) => {
                const state = res ? AuthState.authorized : AuthState.unathorized;
                this.storage.set(this._key, state)
                    .then(() => this.storage.set(this._key, state))
                    .then(() => subject$.next(state));
            },
            null,
            () => subject$.complete()
        );
        return subject$;
    }

    private ifValid() {
        console.log('if valid ' + this.authState$.value);
        this.storage.get(this._key).then((st: AuthState) => {
            console.log(`state : ${st}`);
            if (st === AuthState.authorized) {
                this.authState$.next(AuthState.authorized);
                this.router.navigate(['/home']);
            }
        });
    }


}
