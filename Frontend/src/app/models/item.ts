export class Item {
    id: number;
    brand: string;
    model: string;
    categoryId: number;

    constructor(brand: string, model: string, categoryId: number, id?: number) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.categoryId = categoryId;
    }
}

