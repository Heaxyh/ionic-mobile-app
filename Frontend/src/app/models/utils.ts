export enum AuthState {
    unathorized,
    authorized
}

export interface ILogin {
    username: string;
    password: string;
}
