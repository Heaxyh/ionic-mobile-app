export interface CategoryView {
    visible: boolean;
}

export interface Category extends CategoryView {
    id: number;
    name: string;
    description: string;
}
