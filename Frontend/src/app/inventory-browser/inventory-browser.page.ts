import { Category } from '../models/category';
import { Item } from '../models/item';
import { ItemService } from '../services/inventory-item.service';
import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../services/inventory-category.service';
import { LocalStorageService } from '../services/local-storage.service';
import { NavController } from '@ionic/angular';

@Component({
    selector: 'app-inventory-browser',
    templateUrl: './inventory-browser.page.html',
    styleUrls: ['./inventory-browser.page.scss'],
})
export class InventoryBrowserPage implements OnInit {

    categories: Category[];
    items: Item[];

    constructor(
        private categoryService: CategoryService,
        private itemService: ItemService,
        private navController: NavController,
        private localStorageService: LocalStorageService) {
    }

    ngOnInit() {
        this.categoryService.getCategories().subscribe(
            (res: Category[]) => this.categories = res,
            (err) => console.log(err));

        this.itemService.getItems().subscribe(
            (res: Item[]) => this.items = res,
            (err) => console.log(err));
    }

    getItemsByCategory(id: number) {
        const validItems: Item[] = [];
        this.items.forEach((item) => {
                if (item.categoryId === id) {
                    validItems.push(item);
                }
        });
        return validItems;
    }

    showItems(categoryId: number) {
        this.categories.forEach(cat => {
                if (cat.id === categoryId) {
                    cat.visible = !cat.visible;
                }
        });
    }

    onDelete(item: Item) {
        this.itemService.deleteItem(item.id).subscribe(
            (res: any) => console.log(res),
            (err) => console.log(err),
            () => this.items.splice(this.items.indexOf(item), 1));
    }

    onEdit(item: Item) {
        this.localStorageService.item = item;
        this.navController.navigateForward('/item-editor');
    }

}
