import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InventoryBrowserPageRoutingModule } from './inventory-browser-routing.module';

import { InventoryBrowserPage } from './inventory-browser.page';
import { ItemCategorizerPipe } from './item-categorizer.pipe';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        InventoryBrowserPageRoutingModule
    ],
    declarations: [InventoryBrowserPage, ItemCategorizerPipe]
})
export class InventoryBrowserPageModule {
}
