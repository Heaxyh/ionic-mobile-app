import { Pipe, PipeTransform } from '@angular/core';
import { Item } from '../models/item';
import { Category } from '../models/category';

@Pipe({
  name: 'itemCategorizer'
})
export class ItemCategorizerPipe implements PipeTransform {

  transform(value: Item[], catgories: Category[]): Item[] {
    if (!(value instanceof Array)) {
      return new Array<Item>();
    } else {
      return value.filter(item => catgories.find(cat => cat.id === item.categoryId) ? 1 : -1);
    }
  }

}
