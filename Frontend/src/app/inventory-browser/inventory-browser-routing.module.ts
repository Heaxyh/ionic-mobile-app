import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InventoryBrowserPage } from './inventory-browser.page';

const routes: Routes = [
    {
        path: '',
        component: InventoryBrowserPage
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class InventoryBrowserPageRoutingModule {
}
