import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InventoryBrowserPage } from './inventory-browser.page';

describe('InventoryBrowserPage', () => {
    let component: InventoryBrowserPage;
    let fixture: ComponentFixture<InventoryBrowserPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [InventoryBrowserPage],
            imports: [IonicModule.forRoot()]
        }).compileComponents();

        fixture = TestBed.createComponent(InventoryBrowserPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
