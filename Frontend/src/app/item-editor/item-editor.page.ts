import { ItemService } from '../services/inventory-item.service';
import { Category } from '../models/category';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../services/inventory-category.service';
import { Item } from '../models/item';
import { LocalStorageService } from '../services/local-storage.service';

@Component({
    selector: 'app-item-editor',
    templateUrl: './item-editor.page.html',
    styleUrls: ['./item-editor.page.scss'],
})
export class ItemEditorPage implements OnInit {
    form: FormGroup;
    categories: Category[];
    private editableId: number;
    constructor(
        private fb: FormBuilder,
        private categoryService: CategoryService,
        private  localStorageService: LocalStorageService,
        private itemService: ItemService) {

        this.form = this.fb.group({
            brand: ['', Validators.required],
            model: ['', Validators.required],
            categoryId: ['', Validators.required]
        });

    }

    get brand(): AbstractControl {
        return this.form.get('brand');
    }

    get model(): AbstractControl {
        return this.form.get('model');
    }

    get categoryId(): AbstractControl {
        return this.form.get('categoryId');
    }

    ngOnInit() {
        this.categoryService.getCategories()
            .subscribe(
                (res: Category[]) => this.categories = res,
                (err) => console.log(err),
                () => this.setSelectedItem());
    }

    onSubmit() {
        const item = new Item(this.brand.value, this.model.value, this.categoryId.value, this.editableId);
        if (!this.editableId) {
            this.itemService.postItem(item).subscribe(res => console.log(res));
        } else {
            this.itemService.updateItem(item).subscribe((res: any) => console.log(res));
            this.editableId = null;
        }
        this.form.reset();
    }

    private setSelectedItem(): void {
        const selectedItem: Item = this.localStorageService.item;
        if (selectedItem) {
            this.brand.setValue(selectedItem.brand);
            this.model.setValue(selectedItem.model);
            this.categoryId.setValue(selectedItem.categoryId);
            this.editableId = selectedItem.id;
        }
    }


}
