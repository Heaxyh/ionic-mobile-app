import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ItemEditorPage } from './item-editor.page';

describe('ItemEditorPage', () => {
    let component: ItemEditorPage;
    let fixture: ComponentFixture<ItemEditorPage>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ItemEditorPage],
            imports: [IonicModule.forRoot()]
        }).compileComponents();

        fixture = TestBed.createComponent(ItemEditorPage);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
