import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ItemEditorPageRoutingModule } from './item-editor-routing.module';

import { ItemEditorPage } from './item-editor.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ReactiveFormsModule,
        ItemEditorPageRoutingModule
    ],
    declarations: [ItemEditorPage]
})
export class ItemEditorPageModule {
}
