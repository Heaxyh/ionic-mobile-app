﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryAPI.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InventoryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly InventoryContext _db;
        // POST api/<controller>
        [HttpPost]
        public IActionResult Post([FromBody] AuthCredentialsDTO credentials)
        {
            var valid = _db.Credentialses.Count(x =>
                x.password == credentials.password && x.username == credentials.username);
            if(valid == 1) return Ok(true);
            return ValidationProblem();
        }

        public AuthController(InventoryContext db)
        {
            _db = db;
        }
    }
}