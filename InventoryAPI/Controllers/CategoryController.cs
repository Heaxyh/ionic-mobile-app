﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryAPI.Model;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace InventoryAPI.Controllers
{
    [Route("api/[controller]")]
    public class CategoryController : Controller
    {
        private readonly InventoryContext _db;

        // GET: api/<controller>
        [HttpGet]
        public ActionResult<IEnumerable<CategoryDTO>> Get()
        {
            List<CategoryDTO> categories = new List<CategoryDTO>(); 
            _db.Categories.ToList().ForEach(x=> categories.Add( new CategoryDTO(x) ));
            return Ok(categories);
        }

        //// GET api/<controller>/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<controller>
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}


        public CategoryController(InventoryContext db)
        {
            _db = db;
        }

    }
}
