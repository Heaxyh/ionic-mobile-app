﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventoryAPI.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace InventoryAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private readonly InventoryContext _db;

        [HttpGet]
        public ActionResult<IEnumerable<ItemDTO>> Get()
        {
            List<ItemDTO> items = new List<ItemDTO>();
            _db.Items.ToList().ForEach( x => items.Add(new ItemDTO(x)));
            return Ok(items);
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public ActionResult<ItemDTO> Get(int id)
        {
            var item = _db.Items.First(x => x.Id == id);
            if (item != null) return new ItemDTO(item);
            return NotFound();

        }

        [HttpPut("{id}")]
        public IActionResult Put([FromBody] ItemDTO dto)
        {
            try
            {
                var item = _db.Items.First(x => x.Id == dto.Id);
               item.UpDateItem(dto);
                _db.SaveChanges();
                return Ok(item);
            }
            catch (Exception)
            {
                return ValidationProblem();
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody]  ItemDTO dto)
        {
            try
            {
                var item = new Item(dto);
                _db.Items.Add(item);
                _db.SaveChanges();
                return Ok(item);
            }
            catch (Exception)
            {
                return ValidationProblem();
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = _db.Items.First(x => x.Id == id);
            if (item == null) return NotFound();
            _db.Items.Remove(item);
            _db.SaveChanges();
            return Ok();
        }




        public ItemController(InventoryContext db)
        {
            _db = db;
        }

        

    }
}