﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryAPI.Model
{
    public class Category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get;  set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        private DateTime CreateDateTime { get; set; }

        public Category( string name, string description)
        {
            Name = name;
            Description = description;
            CreateDateTime = DateTime.Now;
            
        }
    }

    public class CategoryDTO
    {

        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public CategoryDTO(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public CategoryDTO(Category category)
        {
            Id = category.Id;
            Name = category.Name;
            Description = category.Description;
        }
    }
}
