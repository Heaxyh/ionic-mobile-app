﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryAPI.Model
{
    public class AuthCredentialsDTO
    {
        public string username { get; set; }
        public string password { get; set; }
    }

    public class AuthCredentials
    {
        [Key]
        public string username { get; set; }
        [MinLength(8)]
        public string password { get; set; }
    }
}
