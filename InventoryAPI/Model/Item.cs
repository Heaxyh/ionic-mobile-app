﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis;

namespace InventoryAPI.Model
{
    public class Item
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Required]
        public string Brand { get; set; }
        [Required]
        public string Model { get; set; }
        [Required]
        public long CategoryId { get; set; }
        [Required]

        private DateTime CreateDate { get; set; }
        public void UpDateItem(ItemDTO item)
        {
            Brand = item.Brand;
            Model = item.Model;
            CategoryId = item.CategoryId;
        }

        public Item(ItemDTO item)
        {
            Brand = item.Brand;
            Model = item.Model;
            CategoryId = item.CategoryId;
            CreateDate = DateTime.Now;
        }

        public Item()
        {
            CreateDate = DateTime.Now;
        }

    }

    public class ItemDTO
    {
        public long Id { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        private DateTime CreationDate { get; set; }
        public long CategoryId { get; set; }

        

        public ItemDTO(long id, string brand, string model, bool active, long categoryId)
        {
            Id = id;
            Brand = brand;
            Model = model;
            CreationDate = DateTime.Now;
            CategoryId = categoryId;
        }


        public ItemDTO(Item item)
        {
            Id = item.Id;
            Brand = item.Brand;
            Model = item.Model;
            CategoryId = item.CategoryId;
        }

       
    }
}
